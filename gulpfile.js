"use strict";

const gulp = require('gulp');
const gp = require('gulp-load-plugins')({
	DEBUG:false,
	pattern : ['imagemin-*', 'gulp-*', 'gulp.*', '@*/gulp{-,.}*', 'postcss-*']
});
//const cache = require('gulp-cache');
const sync = require('browser-sync').create();

const path = {
	build: {
		html:  'build/',
		js:    'build/assets/js/',
		css:   'build/assets/css/',
		img:   'build/assets/img/',
		fonts: 'build/assets/fonts/'
	},
	dev: {
		html:  'dev/*.html',
		js:    'dev/assets/js/*.js',
		css:   'dev/assets/css/*.css',
		img:   'dev/assets/img/**/*.*',
		fonts: 'dev/assets/fonts/**/*.*'
	},
	watch: {
		html:  'dev/**/*.html',
		js:    'dev/assets/js/**/*.js',
		css:   'dev/assets/css/*.css',
		img:   'dev/assets/img/**/*.*',
		fonts: 'dev/assets/fonts/**/*.*'
	},
	clean:   './build'
};

gulp.task('sync', function() {
	sync.init({
		server: {
			baseDir: "./build"
		},
		notify: false
	});
});

gulp.task('html', function () {
	return gulp.src(path.dev.html)
		.pipe(gp.rigger())
		.pipe(gulp.dest(path.build.html))
		.on('end', sync.reload);
});

gulp.task('css', function () {
	//const autoprefixer = require('autoprefixer');
	const cssnano = require('cssnano');
	const mqpacker = require('css-mqpacker');
	const plugins = [
		gp.postcssImport(),
		gp.postcssShort,
		gp.postcssPresetEnv({
			stage: 3,
			features: {
				'nesting-rules': true,
				'autoprefixer': true
			}
		}),
		mqpacker(),
		//autoprefixer({browsers: ['last 2 version']}),
		cssnano()
	];
	return gulp.src(path.dev.css)
		.pipe(gp.postcss(plugins))
		.pipe(gulp.dest(path.build.css))
		.pipe(sync.reload({stream: true}));
});

gulp.task('js', function() {
	return gulp.src(path.dev.js)
		.pipe(gp.rigger())
		.pipe(gulp.dest(path.build.js))
		.pipe(gp.uglify())
		.pipe(gulp.dest(path.build.js))
		.on('end', sync.reload);
});

gulp.task('img', function () {
	return gulp.src(path.dev.img)
		.pipe(
		gp.cache(
			gp.imagemin(
					[
						//png
						gp.imageminPngquant({
							speed: 1,
							quality: [0.8, 0.9],
							strip: true
						}),
						gp.imageminZopfli({
							more: true
							// iterations: 50 // very slow but more effective imagemin-zopfli
						}),
						//gif very light lossy, use only one of gifsicle or Giflossy
						gp.imageminGifsicle({
							optimizationLevel: 3,
						}),
						//svg
						gp.imageminSvgo({
							plugins: [
								{removeViewBox: false}
							]
						}),
						//jpg lossless
						gp.imageminJpegtran({
							progressive: true
						}),
						//jpg very light lossy, use vs jpegtran
						gp.imageminMozjpeg({
							quality: 90
						})
					]
				)
			)
		)
		.pipe(gulp.dest(path.build.img))
		.pipe(gp.imagemin([gp.imageminWebp({method: 6})]))
		.pipe(gp.rename({extname: '.webp'}))
		.pipe(gulp.dest(path.build.img))
		.pipe(sync.reload({stream: true}))
});

gulp.task('watch', function() {
	gulp.watch(path.watch.html, gulp.series('html'));
	gulp.watch(path.watch.css, gulp.series('css'));
	gulp.watch(path.watch.js, gulp.series('js'));
	gulp.watch(path.watch.img, gulp.series('img'));
});

gulp.task('default', gulp.series(
	gulp.parallel('html', 'css', 'js', 'img'),
	gulp.parallel('watch', 'sync')
));

gulp.task('del-cache', () =>
	gp.cache.clearAll()
);